include .env

.PHONY: docker-build start restart stop install composer-install post-install build entity migration migrate fixtures \
load-fixtures cache-clear tests generate-cert-arch cs-fix

EXEC = docker compose exec -u $(USER) app
EXEC_ROOT = docker compose exec app
COMPOSER = $(EXEC) composer

NO_DEV :=
ifeq ($(ENV), 'prod')
	NO_DEV := " --no-dev"
endif

docker-build:
	docker compose build --pull --no-cache

start:
	docker compose up -d

restart:
	$(MAKE) stop
	$(MAKE) start

stop:
	docker compose down --remove-orphans

create:
ifeq ("$(wildcard app/composer.json)", "")
	docker compose stop nginx
ifneq ("$(wildcard app/.env.local)", "")
	mkdir ./tmp
	cp ./app/{.env.local,.env.test.local} ./tmp/
endif
	$(EXEC_ROOT) sh -c 'rm -rf ./* ./.[!.]*'
	git clone git@gitlab.com:Senokossov/symfony-skeleton.git ./app
ifneq ("$(wildcard ./tmp)", "")
	mv ./tmp/{.env.local,.env.test.local} ./app/
	rm -rf tmp
endif
	docker compose up -d nginx
endif

install:
	$(MAKE) create

ifeq ("$(wildcard app/.env.local)", "")
	$(EXEC) touch .env.local
	$(EXEC) sh -c 'echo "DATABASE_URL=\"postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@db:5432/${POSTGRES_DB}?serverVersion=${POSTGRES_VERSION}&charset=utf8\"" >> .env.local'
endif

ifeq ("$(wildcard app/.env.test.local)", "")
	$(EXEC) touch .env.test.local
	$(EXEC) sh -c 'echo "DATABASE_URL=\"postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@db-test:5432/${POSTGRES_DB}?serverVersion=${POSTGRES_VERSION}&charset=utf8\"" >> .env.test.local'
endif

	$(MAKE) composer-install
	$(MAKE) post-install

composer-install:
	$(COMPOSER) install --prefer-dist$(NO_DEV) --no-progress --no-scripts --no-interaction
	$(COMPOSER) dump-autoload --classmap-authoritative$(NO_DEV)
	$(COMPOSER) run-script$(NO_DEV) post-install-cmd
	$(COMPOSER) clear-cache
	$(EXEC) chmod +x bin/console; sync;
ifneq ($(ENV), 'prod')
	$(COMPOSER) install --working-dir=tools/php-cs-fixer
endif

post-install:
ifeq ($(ENV), 'prod')
	$(COMPOSER) symfony:dump-env prod
endif

ifneq ($(ENV), 'prod')
	$(MAKE) recreate-db
endif

ifneq ("$(wildcard app/migrations/Version*)", "")
	$(MAKE) migrate
endif

ifneq ($(ENV), 'prod')
	$(EXEC) bin/console doctrine:fixtures:load
endif

	$(EXEC) bin/console cache:clear

	$(EXEC_ROOT) setfacl -R -m u:www-data:rwX -m u:"$(whoami)":rwX var
	$(EXEC_ROOT) setfacl -dR -m u:www-data:rwX -m u:"$(whoami)":rwX var

recreate-db:
	$(MAKE) restart
	$(EXEC) bin/console doctrine:database:drop --if-exists --force
	$(EXEC) bin/console doctrine:database:drop --env=test --if-exists --force
	$(EXEC) bin/console doctrine:database:create
	$(EXEC) bin/console doctrine:database:create --env=test

build:
	$(MAKE) docker-build
	$(MAKE) start
	$(MAKE) install

entity:
	$(EXEC) bin/console make:entity

migration:
	$(EXEC) bin/console make:migration

migrate:
	$(EXEC) bin/console doctrine:migrations:migrate
	$(EXEC) bin/console doctrine:migrations:migrate --env=test

fixtures:
	$(EXEC) bin/console make:fixtures

load-fixtures:
	$(EXEC) bin/console doctrine:fixtures:load --env=test

cache-clear:
	$(EXEC) bin/console cache:clear

tests:
	$(MAKE) load-fixtures
	$(EXEC) bin/console cache:clear --env=test
	$(EXEC) bin/phpunit

generate-cert-arch:
	SERVER_NAME=$(SERVER_NAME) ./generate-cert-arch.sh

cs-fix:
	$(EXEC) tools/php-cs-fixer/vendor/bin/php-cs-fixer fix
