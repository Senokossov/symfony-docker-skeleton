# Symfony docker skeleton

## Getting Started

Linux is required. You can't run this project on Windows, and may have some problems on macOS \
(but I sure you can fix this and make PR).

1. If not already done, [install Docker Compose](https://docs.docker.com/compose/install/)
2. Copy `.env.dist` to `.env` and change values if you need. \
**Pay special attention to `USER_UID` that determines your system user uid. \
If you are a single user on the machine, that might be 1000, so you shouldn't change this variable. \
Otherwise, it can be different. \
You can check it by shell command `id`**
3. If you need HTTPS, go to *Set up HTTPS* and then continue next steps. \
If not, remove `NGINX_CONFIG` from .env or set it to `default`.
4. Run `make build` to build, start and install the project

## Set up HTTPS
If you already have got trusted SSL certificates
1. Put them into docker/nginx/ssl
2. Set actual paths to certificates in https.conf
3. Return to step 4 of *Getting Started*.

Otherwise, you can generate self-signed trusted certificate on your host. But it depends on your distro. \
On Arch-based distros run `make generate-cert-arch` from project root directory. Others can be different.

## Commands

`make stop` to stop the project  
`make start` to start the project again  
`make restart` to restart the project  
`make install` run after each git pulling or branch switching (includes composer install, doctrine migrate etc.).
**Note that both databases, dev and test, will be dropped and recreated**  
`make entity` to make symfony entity  
`make migration` to generate new migration from entity(-ies)  
`make migrate` to execute doctrine migration  
`make fixtures` to make doctrine fixtures  
`make load-fixtures` to load fixtures into the database  
`make cache-clear` to clear Symfony cache  
`make tests` to run all tests  
`make cs-fix` to code style fixing due to PSR-12 and Symfony CS \
(except Yoda conditions, no-space around concatenation operator; \
although disabled the rule curly_braces_position due to moving closing curly brace to the next line in an empty constructor)

`docker compose exec -u $USER app bin/console uuid:generate` to manually generate UUID for mocks

`docker compose exec -u $USER app {command}` to execute any shell command inside php docker container, \
e.g. `docker compose exec -u $USER app composer require --dev symfony/test-pack`
