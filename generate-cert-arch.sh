#!/usr/bin/env bash
set -eu
org="$SERVER_NAME"-ca
domain="$SERVER_NAME"
dir=docker/nginx/ssl

sudo trust anchor --remove $dir/ca.crt || true

openssl genpkey -algorithm RSA -out $dir/ca.key
openssl req -x509 -key $dir/ca.key -out $dir/ca.crt \
    -subj "/CN=$org/O=$org"

openssl genpkey -algorithm RSA -out $dir/"$domain".key
openssl req -new -key $dir/"$domain".key -out $dir/"$domain".csr \
    -subj "/CN=$domain/O=$org"

openssl x509 -req -in $dir/"$domain".csr -days 365 -out $dir/"$domain".crt \
    -CA $dir/ca.crt -CAkey $dir/ca.key -CAcreateserial \
    -extfile <(cat <<END
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
subjectAltName = DNS:$domain
END
    )

sudo trust anchor $dir/ca.crt
