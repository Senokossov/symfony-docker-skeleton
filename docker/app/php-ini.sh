#!/bin/sh
PHP_INI_RECOMMENDED="$PHP_INI_DIR/php.ini-production"
if [ "$ENV" != 'prod' ]; then
	PHP_INI_RECOMMENDED="$PHP_INI_DIR/php.ini-development"
fi
ln -sf "$PHP_INI_RECOMMENDED" "$PHP_INI_DIR/php.ini"
