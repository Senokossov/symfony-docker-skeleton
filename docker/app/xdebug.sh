#!/bin/sh
if [ "$ENV" != "prod" ]; then
	set -eux; \
	apk add --no-cache --virtual .build-deps $PHPIZE_DEPS linux-headers; \
	pecl install xdebug-3.2.0; \
	docker-php-ext-enable xdebug; \
	apk del .build-deps;
fi
